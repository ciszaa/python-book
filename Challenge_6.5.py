def invest(amount, rate, years):
    for year in range(1, years + 1):
        amount = amount + (amount * rate)
        print(f"year {year}: ${amount:,.2f}")

amount = input("Enter an initial amount: ")
rate = input("Enter an annual interest rate in the decimal system: ")
years = input("Enter a number of years: ")


invest(float(amount), float(rate), int(years))

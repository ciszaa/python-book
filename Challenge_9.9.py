cats = []

for number_of_cat in range(1, 101):
    cats.append([f"Cat {number_of_cat}", False])

for round in range(1, 101):  
    for cat_index in range(round - 1, 100, round):
        cats[cat_index][1] = not cats[cat_index][1]

for cat in cats:
    if cat[1] == True:
        print(cat[0])

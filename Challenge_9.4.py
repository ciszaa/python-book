universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]

def enrollment_stats(universities):
    """Sum of rolled students and tuition fees"""
    rolled_students = [] 
    tuition_fees = []
    rolled_students = [uni[1] for uni in universities]
    tuition_fees = [uni[2] for uni in universities]
    total_rolled_students = sum(rolled_students)
    total_tuition_fees = sum(tuition_fees)
    return total_rolled_students, total_tuition_fees

def mean(universities):
    """Mean of rolled students and tuition fees """
    rolled_students = [uni[1] for uni in universities]
    tuition_fees = [uni[2] for uni in universities]
    mean_rolled_students = round(sum(rolled_students) / len(universities), 2)
    mean_tuition_fees = round(sum(tuition_fees) / len(universities), 2)
    return mean_rolled_students, mean_tuition_fees


def median(universities):
    """Median of rolled students and tuition fees"""
    rolled_students = [uni[1] for uni in universities]
    tuition_fees = [uni[2] for uni in universities]
    rolled_students.sort()
    tuition_fees.sort()
    n = len(rolled_students)
    l = len(tuition_fees)
    if n % 2 == 0:
        median_left_students = rolled_students[n // 2 - 1]
        median_right_students = rolled_students[n // 2]
        median_rolled_students = (median_left_students + median_right_students) / 2        
    else:
        median_rolled_students = rolled_students[n // 2]
    if l % 2 == 0:
        median_left_tuition_fees = tuition_fees[l // 2 - 1]
        median_right_tuition_fees = tuition_fees[l // 2]
        median_tuition_fees = (median_left_tuition_fees + median_right_tuition_fees) / 2
    else:
        median_tuition_fees = tuition_fees[l // 2]
    return median_rolled_students, median_tuition_fees

totals = enrollment_stats(universities)
mean = mean(universities)
median = median(universities)

print("******************************")
print(f"Total students: {totals[0]:,}")
print(f"Total tuition: $ {totals[1]:,}")
print()
print(f"Student mean: {mean[0]:,}")
print(f"Student median: {median[0]:,}")
print()
print(f"Tuition mean: $ {mean[1]:,}")
print(f"Tuition median: $ {median[1]:,}")
print("******************************")

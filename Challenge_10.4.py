class Animal:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def walk(self):
        return f"{self.name} is walking."

    def eat(self, food):
        return f"{self.name} eats {food}"

    def sleep(self):
        return f"{self.name} is sleeping."


class Dog(Animal):
    
    def __init__(self, name, age, sound):
        super().__init__(name, age)
        self.sound = sound

    def bark(self):
        return f"{self.name} barks {self.sound}"


class Chicken(Animal):

    def __init__(self, name, age, size_eggs):
        super().__init__(name, age)
        self.size_eggs = size_eggs

    def lay_eggs(self):
        return f"{self.name} lays eggs in size {self.size_eggs}."


class Cow(Animal):
    def __init__(self, name, age, belly_color, age_milk_production):
        super().__init__(name, age)
        self.belly_color = belly_color
        self.age_milk_production = age_milk_production

    def yields_milk(self, litres):
        return f"{self.name} yields {litres} litres of milk."

dog = Dog("Burek", 2, "Woof!")
print(f"Dog's name is {dog.name}. He's {dog.age} years old.")
print(dog.bark())

chicken = Chicken("Rosół", 2, "M")
print(f"\nChicken's name is {chicken.name}. He's {chicken.age} years old.")
print(chicken.lay_eggs())

cow = Cow("Mućka", 4, "red", 2)
print(f"\nCow's name is {cow.name}. She has {cow.belly_color} belly and produces milk when she was {cow.age_milk_production} years old.")
print(cow.yields_milk(1))

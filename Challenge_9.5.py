Nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
Verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
Adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
Prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
Adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

import random

noun1 = random.choice(Nouns)
noun2 = random.choice(Nouns)
noun3 = random.choice(Nouns)

verb1 = random.choice(Verbs)
verb2 = random.choice(Verbs)
verb3 = random.choice(Verbs)

adj1 = random.choice(Adjectives)
adj2 = random.choice(Adjectives)
adj3 = random.choice(Adjectives)

prep1 = random.choice(Prepositions)
prep2 = random.choice(Prepositions)

adv = random.choice(Adverbs)

if adj1.startswith("i") or adj1.startswith("e"):
    article = "An"
else:
    article = "A"

print(f"""
      {article} {adj1} {noun1}\n
      {article} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}
      {adv}, the {noun1} {verb2}
      the {noun2} {verb3} {prep2} a {adj3} {noun3}\n
    """)

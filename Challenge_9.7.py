import random

capitals_dict = {
    "Alabama": "Montgomery",
    "Alaska": "Juneau",
    "Arizona": "Phoenix",
    "Arkansas": "Little Rock",
    "California": "Sacramento",
    "Colorado": "Denver",
    "Connecticut": "Hartford",
    "Delaware": "Dover",
    "Florida": "Tallahassee",
    "Georgia": "Atlanta",
    }

state, capital = random.choice(list(capitals_dict.items()))

while True:
    guess_capital = input(f"Enter a capital of {state}: ").lower()
    if guess_capital == "exit":
        print(f"The capital of {state} is {capital}.")
        print("Goodbye!")
        break
    elif guess_capital == capital.lower():
        print("Correct!")
        break



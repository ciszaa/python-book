temp_far = input("Enter a temperature in degrees F: ")

def convert_far_to_cel(temp_far):
    temp_cel = (float(temp_far) - 32) * 5/9
    return temp_cel

temp_cel = convert_far_to_cel(temp_far)
print(f"{temp_far} degrees F = {temp_cel:.2f} degrees C")


temp_cel = input("Enter a temperature in degrees C: ")

def convert_cel_to_far(temp_cel):
    temp_far = float(temp_cel) * 9/5 + 32
    return temp_far

temp_far = convert_cel_to_far(temp_cel)
print(f"{temp_cel} degrees C = {temp_far:.2f} degrees F")
